# KTANE Solver
###### Helps you solving [Keep talking and nobody explodes][ktane-game]

An application for solving puzzles from [KTANE][ktane-game] without surfing through the manual. You have to read the [manual][ktane-manual] first for having a basic knowledge of the game and modules.

### Contributors
* Joshua Behrens <code@joshua-behrens.de> / [Website][contrib-website-a]

### License
> Copyright (c) 2015 Joshua Behrens
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> The Software shall **not** be used to create products or support military actions that are intended to harm any human being
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

tl;dr: Refer to the main contributors and do not harm any people

### Trivia
The idea came on new years party 2016 while playing KTANE for hours with my friends.

[ktane-game]: //keeptalkinggame.com "Keep talking and nobody explodes"
[ktane-manual]: //bombmanual.com "Bomb defusing manual"
[contrib-website-a]: //joshua-behrens.de "Website of Joshua Behrens"