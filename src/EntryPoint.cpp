#include <deque>
#include <iostream>
#include <algorithm>

#include "UI.hpp"

void PuzzleWires()
{
	const int RED = 0, BLUE = 1, YELLOW = 2, BLACK = 3, WHITE = 4;

	auto wires = UI::OptionsList("List up the wires in their occurence: ", {"Red", "Blue", "Yellow", "Black", "White"}, 3, 6);

	auto cut = [](int pos)
	{
		std::cout << "Cut the wire in position " << pos << std::endl;
	};

	int odd = -1;
	auto isOdd = [&odd]()
	{
		if (odd == -1)
		{
			odd = UI::Options("Is the last digit of the serial number odd?", {"No", "Yes"});
		}

		return odd == 1;
	};

	switch (wires.size())
	{
		case 3:
			if (std::find(wires.begin(), wires.end(), RED) == wires.end())
			{
				cut(2);
			}
			else if (wires.back() == WHITE)
			{
				cut(3);
			}
			else if (std::count_if(wires.begin(), wires.end(), [](int i) { return i == BLUE; }) > 1)
			{
				cut((int)std::distance(wires.begin(), std::upper_bound(wires.begin(), wires.end(), BLUE)));
			}
			else
			{
				cut(3);
			}
			break;
		case 4:
			if ((std::count_if(wires.begin(), wires.end(), [](int i) { return i == RED; }) > 1) && isOdd())
			{
				cut((int)std::distance(wires.begin(), std::upper_bound(wires.begin(), wires.end(), RED)));
			}
			else if ((wires.back() == YELLOW) && (std::count_if(wires.begin(), wires.end(), [](int i) { return i == RED; }) == 0))
			{
				cut(1);
			}
			else if (std::count_if(wires.begin(), wires.end(), [](int i) { return i == BLUE; }) == 1)
			{
				cut(1);
			}
			else if (std::count_if(wires.begin(), wires.end(), [](int i) { return i == YELLOW; }) > 1)
			{
				cut(4);
			}
			else
			{
				cut(2);
			}
			break;
		case 5:
			if ((wires.back() == BLACK) && isOdd())
			{
				cut(4);
			}
			else if ((std::count_if(wires.begin(), wires.end(), [](int i) { return i == RED; }) == 1) &&
				     (std::count_if(wires.begin(), wires.end(), [](int i) { return i == YELLOW; }) > 2))
			{
				cut(1);
			}
			else if (std::count_if(wires.begin(), wires.end(), [](int i) { return i == BLACK; }) == 0)
			{
				cut(2);
			}
			else
			{
				cut(1);
			}
			break;
		case 6:
			if ((std::count_if(wires.begin(), wires.end(), [](int i) { return i == YELLOW; }) == 0) && isOdd())
			{
				cut(3);
			}
			else if ((std::count_if(wires.begin(), wires.end(), [](int i) { return i == YELLOW; }) == 1) &&
					 (std::count_if(wires.begin(), wires.end(), [](int i) { return i == WHITE; }) > 1))
			{
				cut(4);
			}
			else if (std::count_if(wires.begin(), wires.end(), [](int i) { return i == RED; }) == 0)
			{
				cut(6);
			}
			else
			{
				cut(4);
			}
			break;
	}

	UI::WaitForInput();
}

void PuzzleButton()
{

}

void PuzzleKeypads()
{

}

void PuzzleSimonSays()
{

}

void PuzzleWhosOnFirst()
{

}

void PuzzleMemory()
{
	int memLabel[5];
	int memPos[5];

	for (int c = 0; c < 5; ++c)
	{
		memLabel[c] = -1;
		memPos[c] = -1;
	}

	auto pressLabel = [&memPos, &memLabel](int label, int step)
	{
		std::cout << "Press the label " << (memLabel[step] = label) << std::endl;

		// Do not ask if it is the last step
		if (step < 4)
		{
			memPos[step] = UI::Options("In which position was the pressed button?", {"1", "2", "3", "4"}) + 1;
		}
	};
	auto pressPosition = [&memPos, &memLabel](int position, int step)
	{
		std::cout << "Press the position " << (memPos[step] = position) << std::endl;

		// Do not ask if it is the last step
		if (step < 4)
		{
			memLabel[step] = UI::Options("Which label had the pressed button?", {"1", "2", "3", "4"}) + 1;
		}
	};

	for (int c = 0; c < 5; ++c)
	{
		switch (UI::Options("What does the display say?", {"1", "2", "3", "4", "Player failed / Bomb exploded"}))
		{
			case 0: // Display 1
				switch (c)
				{
					case 0: // Stage 1
						pressPosition(2, c);
						break;
					case 1: // Stage 2
						pressLabel(4, c);
						break;
					case 2: // Stage 3
						pressLabel(memLabel[1], c);
						break;
					case 3: // Stage 4
						pressPosition(memPos[0], c);
						break;
					case 4: // Stage 5
						pressLabel(memLabel[0], c);
						break;
				}
				break;
			case 1: // Display 2
				switch (c)
				{
					case 0: // Stage 1
						pressPosition(2, c);
						break;
					case 1: // Stage 2
						pressPosition(memPos[0], c);
						break;
					case 2: // Stage 3
						pressLabel(memLabel[0], c);
						break;
					case 3: // Stage 4
						pressPosition(1, c);
						break;
					case 4: // Stage 5
						pressLabel(memLabel[1], c);
						break;
				}
				break;
			case 2: // Display 3
				switch (c)
				{
					case 0: // Stage 1
						pressPosition(3, c);
						break;
					case 1: // Stage 2
						pressPosition(1, c);
						break;
					case 2: // Stage 3
						pressPosition(3, c);
						break;
					case 3: // Stage 4
						pressPosition(memPos[1], c);
						break;
					case 4: // Stage 5
						pressLabel(memLabel[3], c);
						break;
				}
				break;
			case 3: // Display 4
				switch (c)
				{
					case 0: // Stage 1
						pressPosition(4, c);
						break;
					case 1: // Stage 2
						pressPosition(memPos[0], c);
						break;
					case 2: // Stage 3
						pressLabel(4, c);
						break;
					case 3: // Stage 4
						pressPosition(memPos[1], c);
						break;
					case 4: // Stage 5
						pressLabel(memLabel[2], c);
						break;
				}
				break;
			default: // Something went wrong
				return;
		}
	}
}

void PuzzleMorseCode()
{

}

void PuzzleComplicatedWires()
{

}

void PuzzleSequencedWires()
{
	const int A = 0x1 << 1;
	const int B = 0x1 << 2;
	const int C = 0x1 << 3;

	int reds[] =
	{
		        C,     B,     A,
		    A | C,     B, A | C,
		A | B | C, A | B,     B
	};
	int blues[] =
	{
		B, A | C,     B,
		A,     B, B | C,
		C, A | C,     A
	};
	int blacks[] =
	{
		A | B | C, A | C,     B,
	        A | C,     B, B | C,
	        A | B,     C,     C
	};

	int *red = reds, *blu = blues, *bla = blacks;


	bool allNeededWires = false;
	while (!allNeededWires)
	{
		int wire = UI::Options("Next wire information: ", {"Red A", "Red B", "Red C",
		                                                   "Blue A", "Blue B", "Blue C",
		                                                   "Black A", "Black B", "Black C",
		                                                   "Bomb is exploded / Puzzle finished / Player failed"});

		switch (wire)
		{
			case 0: // RED A
				std::cout << ((*(red++) & A) ? "Cut" : "Stay") << std::endl;
				break;
			case 1: // RED B
				std::cout << ((*(red++) & B) ? "Cut" : "Stay") << std::endl;
				break;
			case 2: // RED C
				std::cout << ((*(red++) & C) ? "Cut" : "Stay") << std::endl;
				break;
			case 3: // BLUE A
				std::cout << ((*(blu++) & A) ? "Cut" : "Stay") << std::endl;
				break;
			case 4: // BLUE B
				std::cout << ((*(blu++) & B) ? "Cut" : "Stay") << std::endl;
				break;
			case 5: // BLUE C
				std::cout << ((*(blu++) & C) ? "Cut" : "Stay") << std::endl;
				break;
			case 6: // BLACK A
				std::cout << ((*(bla++) & A) ? "Cut" : "Stay") << std::endl;
				break;
			case 7: // BLACK B
				std::cout << ((*(bla++) & B) ? "Cut" : "Stay") << std::endl;
				break;
			case 8: // BLACK C
				std::cout << ((*(bla++) & C) ? "Cut" : "Stay") << std::endl;
				break;
			default:
				allNeededWires = true;
				break;
		}

		UI::WaitForInput();
	}
}

void PuzzleMazes()
{

}

void PuzzlePasswords()
{
	std::deque<std::string> passwords =
	{
		"about", "after", "again", "below", "could",
        "every", "first", "found", "great", "house",
	    "large", "learn", "never", "other", "place",
		"plant", "point", "right", "small", "sound",
		"spell", "still", "study", "their", "there",
		"these", "thing", "think", "three", "water",
		"where", "which", "world", "would", "write"
	};

	int wheels = UI::Options("How many wheels you want to enter?", {"1", "2", "3", "4", "5", "None"});
	std::deque<std::string> filter;

	if (wheels < 5)
	{
		for (int w = 0; w <= wheels; ++w)
		{
			char question[32];
			sprintf(question, "List all characters for wheel %i: ", w + 1);
			std::string chars = UI::FreeInput(std::string(question));
			std::transform(chars.begin(), chars.end(), chars.begin(), ::tolower);
			filter.push_back(chars);
		}
	}

	for (std::size_t f = 0; f < filter.size(); ++f)
	{
		passwords.erase(std::remove_if(passwords.begin(),
		                               passwords.end(),
		                               [f, &filter](std::string i)
		                               {
			                               return filter[f].find(i.at(f)) == std::string::npos;
		                               }), passwords.end());
	}

	std::cout << "These are the possible passwords: " << std::endl;
	for (auto&& f : passwords)
	{
		std::cout << f << std::endl;
	}
	UI::WaitForInput();
}

void PuzzleNeedyKnobs()
{
	struct LEDConfigs
	{
		bool bits[12];

		// number of clicks on the pointer to rotate from up
		char direction;
	};

	LEDConfigs configs[8] =
	{
		{{0, 0, 1,  0, 1, 1,  1, 1, 1,  1, 0, 1}, 0},
		{{1, 0, 1,  0, 1, 0,  0, 1, 1,  0, 1, 1}, 0},
		{{0, 1, 1,  0, 0, 1,  1, 1, 1,  1, 0, 1}, 2},
		{{1, 0, 1,  0, 1, 0,  0, 1, 0,  0, 0, 1}, 2},
		{{0, 0, 0,  0, 1, 0,  1, 0, 0,  1, 1, 1}, 3},
		{{0, 0, 0,  0, 1, 0,  0, 0, 0,  1, 1, 0}, 3},
		{{1, 0, 1,  1, 1, 1,  1, 1, 1,  0, 1, 0}, 1},
		{{1, 0, 1,  1, 0, 0,  1, 1, 1,  0, 1, 0}, 1}
	};

	auto leds = UI::OptionsList("List the states of the LEDs:", {"Off", "On"}, 1, 12);
	std::string directions[4] = {"Up", "Right", "Down", "Left"};
	char bitOutput[2] = {' ', 'X'};

	std::cout << "These are all possible solutions:" << std::endl;

	for (int c = 0; c < 8; ++c)
	{
		if (std::equal(leds.begin(), leds.end(), configs[c].bits))
		{
			bool* bits = configs[c].bits;
			std::cout << std::endl << "Direction: " << directions[configs[c].direction] << std::endl;
			std::cout << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*(bits++)] << "|  ";
			std::cout << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*(bits++)] << '|' << std::endl;
			std::cout << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*(bits++)] << "|  ";
			std::cout << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*(bits++)] << '|' << bitOutput[*bits] << '|' << std::endl;
		}
	}

	UI::WaitForInput();
}

int main()
{
	std::deque<void(*)()> actions;

	actions.push_back(PuzzleWires);
	actions.push_back(PuzzleButton);
	actions.push_back(PuzzleKeypads);
	actions.push_back(PuzzleSimonSays);
	actions.push_back(PuzzleWhosOnFirst);
	actions.push_back(PuzzleMemory);
	actions.push_back(PuzzleMorseCode);
	actions.push_back(PuzzleComplicatedWires);
	actions.push_back(PuzzleSequencedWires);
	actions.push_back(PuzzleMazes);
	actions.push_back(PuzzlePasswords);
	actions.push_back(PuzzleNeedyKnobs);

	int action;

	do
	{
		action = UI::Options("Which puzzle do you need to solve?",
	                         {
		                         "Wires",
		                         "Button",
		                         "Keypads / hieroglyphics",
		                         "Simon says",
		                         "Who's on first",

		                         "Memory",
		                         "Morse code",
		                         "Complicated wires",
		                         "Sequenced wires",
		                         "Mazes",

		                         "Passwords",
		                         "Needy - Knobs",
		                         "Bomb exploded / is finished"
	                         });
		if (action < actions.size())
		{
			actions[action]();
		}
	}
	while (action != actions.size());

	return 0;
}