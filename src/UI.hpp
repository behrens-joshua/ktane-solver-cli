#include <string>
#include <list>

namespace UI
{
	int Options(std::string message, std::initializer_list<std::string> params);
	std::list<int> OptionsList(std::string message, std::initializer_list<std::string> params, unsigned minItems = 1, unsigned maxItems = -1);
	std::string FreeInput(std::string message);
	void WaitForInput();
}