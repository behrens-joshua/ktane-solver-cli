#include <iostream>
#include <cassert>
#include <string.h>

#include "UI.hpp"

const char* ANSWER_CHARS = "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

int __amount_of_possible_answers()
{
	return 9 + 26 + 26;
}

char __char_for_index(int index)
{
	if (0 <= index && index < __amount_of_possible_answers())
	{
		return ANSWER_CHARS[index];
	}

	return 0;
}

int __index_of_char(char c)
{
	return (int)(strchr(ANSWER_CHARS, c) - ANSWER_CHARS);
}

void __list_options(std::initializer_list<std::string> options)
{
	assert(options.size() < (9 + 26 + 26));

	int pos = 0;

	for (auto &&opt : options)
	{
		std::cout << " * [" << __char_for_index(pos++) << "] " << opt << std::endl;
	}
}

int UI::Options(std::string message, std::initializer_list<std::string> options)
{
	int result = -1;

	do
	{
		std::cout << message << std::endl;
		__list_options(options);

		std::string input;
		std::getline(std::cin, input);

		if (input.size() == 1)
		{
			char first = *(input.begin());
			int pos = __index_of_char(first) ;

			if (0 <= pos && pos < options.size())
			{
				return pos;
			}
			else
			{
				std::cout << "Invalid answer" << std::endl;
			}
		}
		else
		{
			std::cout << "Invalid answer" << std::endl;
		}
	}
	while (result <= 0);

	return result;
}


std::list<int> UI::OptionsList(std::string message, std::initializer_list<std::string> options, unsigned minItems, unsigned maxItems)
{
	std::list<int> result;

	while (true)
	{
		std::cout << message << std::endl;
		__list_options(options);

		std::string input;
		std::getline(std::cin, input);

		if (minItems <= input.size())
		{
			if (input.size() <= maxItems)
			{
				bool valid_list = true;

				for (auto&& item : input)
				{
					int pos = __index_of_char(item);

					if (0 <= pos && pos < options.size())
					{
						result.push_back(pos);
					}
					else
					{
						std::cout << "Invalid answer " << item << std::endl;
						result.clear();
						valid_list = false;
						break;
					}
				}

				if (valid_list)
				{
					break;
				}
			}
			else
			{
				std::cout << "You can not choose more than " << minItems << " answers" << std::endl;
			}
		}
		else
		{
			std::cout << "You need at least " << minItems << " answers" << std::endl;
		}
	}

	return result;
}

std::string UI::FreeInput(std::string message)
{
	std::string result;

	do
	{
		std::cout << message << std::endl;
		std::getline(std::cin, result);

		if (result.size() > 0)
		{
			break;
		}
	}
	while (true);

	return result;
}

void UI::WaitForInput()
{
	std::cout << "Press enter to continue" << std::endl;
	std::string l;
	std::getline(std::cin, l);
}